# MTTP Analysis

This repository contains a set of scripts used to analyse GitLab's Mean Time To
Production, as part of issue https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/773.

## Requirements

* Ruby 2.6 or newer
* An API token for GitLab.com, ops.gitlab.net, and dev.gitlab.org
* Bundler

## Usage

First install all dependencies:

    gem install bundler
    bundle install

Next, download all the data files:

    env COM_TOKEN='...' OPS_TOKEN='...' DEV_TOKEN='...' ruby bin/download.rb

If you also want to include data from when Release Tools was still running on
GitLab.com, also run the following:

    env RELEASE_TOOLS_USE_COM='true' COM_TOKEN='...' OPS_TOKEN='...' \
        DEV_TOKEN='...' ruby bin/download.rb

This may take a while and possibly fail half way through, depending on how well
the GitLab APIs behave. If this fails you can just re-run the script, as it will
skip data already downloaded.

Once the data is downloaded you can run the other scripts to present the data.
