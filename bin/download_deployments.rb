# frozen_string_literal: true
# rubocop: disable all
#
# This script downloads all deployments from the GitLab.com API along with their
# deployed merge requests, without relying on any CI pipelines.

require 'etc'
require 'gitlab'
require 'json'
require 'parallel'
require 'time'

# The project to use for retrieving deployments.
DEPLOYMENTS_PROJECT = 'gitlab-org/gitlab'

# The environments to get deployments for.
ENVIRONMENTS = %w[gstg gprd-cny gprd]

CORES = Etc.nprocessors

COM_CLIENT = Gitlab::Client.new(
  endpoint: 'https://gitlab.com/api/v4',
  private_token: ENV.fetch('COM_TOKEN')
)

def deployed_merge_requests(project, deployment_id)
  project_path = COM_CLIENT.url_encode(project)

  COM_CLIENT.get(
    "/projects/#{project_path}/deployments/#{deployment_id}/merge_requests",
    query: { per_page: 100 }
  )
end

data_dir = File.join(Dir.pwd, 'data', 'deployments')
min_date = Date.today - 90

ENVIRONMENTS.each do |env|
  deployments = COM_CLIENT.deployments(
    DEPLOYMENTS_PROJECT,
    per_page: 100,
    sort: 'desc',
    environment: env,
    status: 'success',
    updated_after: min_date.iso8601
  )

  while deployments
    Parallel.each(deployments, in_threads: CORES) do |deployment|
      path = File.join(data_dir, "#{deployment.id}.json")
      mrs = []

      deployed_merge_requests(DEPLOYMENTS_PROJECT, deployment.id)
        .auto_paginate do |mr|
          mrs << {
            id: mr.iid,
            title: mr.title,
            url: mr.web_url,
            created_at: mr.created_at,
            merged_at: mr.merged_at || mr.updated_at,
            source_project_id: mr.source_project_id,
            target_project_id: mr.target_project_id,
            merge_commit_sha: mr.merge_commit_sha
          }
        end

      for_json = {
        id: deployment.id,
        iid: deployment.iid,
        environment: {
          id: deployment.environment.id,
          name: deployment.environment.name
        },
        created_at: deployment.created_at,
        updated_at: deployment.updated_at,
        merge_requests: mrs
      }

      File.open(path, 'w+') do |file|
        file.write(JSON.pretty_generate(for_json))
      end
    end

    deployments = deployments.next_page
  end
end
