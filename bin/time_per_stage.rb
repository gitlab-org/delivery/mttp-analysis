# frozen_string_literal: true
# rubocop: disable all
#
# This script displays the total time spent in the various deployment stages,
# such as building a package and deploying it to production.

require 'json'
require 'time'

def to_hours(seconds)
  seconds / 3600.0
end

def first_stage_job_time(jobs, stage)
  job = jobs
    .select { |job| job['stage'] == stage }
    .sort_by { |job| job['finished_at'] }
    .first

  Time.parse(job['started_at'])
end

def last_stage_job_time(jobs, stage)
  job = jobs
    .select { |job| job['stage'] == stage }
    .sort_by { |job| job['finished_at'] }
    .last

  Time.parse(job['finished_at'])
end

totals = {
  inclusion: 0.0,
  package: 0.0,
  staging: 0.0,
  canary: 0.0,
  promotion: 0.0,
  production: 0.0
}

Dir['./data/pipelines/*.json'].each do |path|
  data = JSON.load(File.read(path))
  deployer = data['deployer']

  next unless deployer['status'] == 'success'

  tag_finished_at = Time.parse(data['release_tools']['finished_at'])
  package_finished_at = Time.parse(data['omnibus']['finished_at'])
  deploy_finished_at = Time.parse(deployer['finished_at'])
  staging_finished_at = last_stage_job_time(deployer['jobs'], 'gstg-finish')
  canary_finished_at = last_stage_job_time(deployer['jobs'], 'gprd-cny-finish')
  production_started_at =
    first_stage_job_time(deployer['jobs'], 'production-promote')

  deployer['merge_requests'].each do |mr|
    merged_at = Time.parse(mr['merged_at'])
    time_to_inclusion = tag_finished_at - merged_at
    time_to_package = package_finished_at - tag_finished_at
    time_to_staging = staging_finished_at - package_finished_at
    time_to_canary = canary_finished_at - staging_finished_at
    time_to_promote = production_started_at - canary_finished_at
    time_to_production = deploy_finished_at - production_started_at

    totals[:inclusion] += time_to_inclusion
    totals[:package] += time_to_package
    totals[:staging] += time_to_staging
    totals[:canary] += time_to_canary
    totals[:promotion] += time_to_promote
    totals[:production] += time_to_production
  end
end

total = totals.values.sum

totals.each do |key, seconds|
  percentage = ((seconds / total) * 100.0)
  blocks = '▮' * percentage.ceil
  label = key.to_s.ljust(12, ' ')

  puts "#{label} #{blocks} #{percentage.round(2)}%"
end
