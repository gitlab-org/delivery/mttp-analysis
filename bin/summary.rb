# frozen_string_literal: true
# rubocop: disable all
#
# This script prints a summary of all deployment pipelines, how long they took,
# and displays a histogram for the deployment statuses and timings.

require 'json'
require 'time'

def to_hours(seconds)
  seconds / 3600.0
end

buckets = {}
max_bucket = 200
bucket_size = 10

(0..max_bucket).step(bucket_size).each do |step|
  next if step.zero?

  buckets[(step - bucket_size)..step] = 0
end

buckets[max_bucket..1000] = 0

per_status = Hash.new(0)

Dir['./data/pipelines/*.json'].each do |path|
  data = JSON.load(File.read(path))
  status = data['deployer']['status']

  next if status == 'manual'

  started_at = Time.parse(data['release_tools']['started_at'])
  finished_at = Time.parse(data['deployer']['finished_at'])
  time_to_prod = to_hours(finished_at - started_at)

  buckets.each do |bucket, _|
    next unless bucket.cover?(time_to_prod)

    buckets[bucket] += 1
    break
  end

  per_status[status] += 1

  puts "#{data['deployer']['url']} (#{status}): #{time_to_prod.round(2)} hours"
end

puts

total = buckets.values.sum.to_f

buckets.each do |bucket, count|
  next if count.zero?

  percentage = ((count.to_f / total) * 100).ceil
  blocks = '▮' * percentage
  label = "#{bucket}h".ljust(12, ' ')

  puts "#{label} #{blocks} #{count}"
end

puts

per_status.each do |status, total|
  blocks = '▮' * total
  label = status.ljust(12, ' ')

  puts "#{label} #{blocks} #{total}"
end
