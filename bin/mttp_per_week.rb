# frozen_string_literal: true
# rubocop: disable all
#
# This script displays the total MTTP per week, and indicates if a security
# release took place during a given week.

require 'json'
require 'time'
require 'gitlab'
require 'set'

# The project used for (security) release tasks.
RELEASE_TASKS_PROJECT = 'gitlab-org/release/tasks'

com_client = Gitlab::Client.new(
  endpoint: 'https://gitlab.com/api/v4',
  private_token: ENV.fetch('COM_TOKEN')
)

per_week_time = Hash.new(0.0)
per_week_count = Hash.new(0.0)
per_week_avg = Hash.new(0.0)
first_date = nil

Dir['./data/pipelines/*.json'].each do |path|
  data = JSON.load(File.read(path))
  deployer = data['deployer']

  next unless deployer['status'] == 'success'

  finished_at = Time.parse(deployer['finished_at'])
  week = finished_at.to_date.cweek

  if !first_date || finished_at < first_date
    first_date = finished_at
  end

  deployer['merge_requests'].each do |mr|
    mttp = finished_at - Time.parse(mr['merged_at'])
    per_week_time[week] += mttp
    per_week_count[week] += 1
  end
end

total_mttp = per_week_time.values.sum.to_f
sorted = per_week_time.sort_by { |(key, _)| key }
min_week = sorted[0][0]
max_week = sorted[-1][0]
filled_time = Hash.new(0)
filled_count = Hash.new(0)
security_weeks = Set.new

com_client
  .issues(
    RELEASE_TASKS_PROJECT,
    labels: 'security,Monthly Release',
    created_after: first_date.iso8601,
    confidential: true
  )
  .auto_paginate do |issue|
    # Issues may be created ahead of time or closed later. So instead of relying
    # on creating/closing dates, we rely on when the first/last TODOs are marked
    # as done.
    system_notes = com_client
      .issue_notes(RELEASE_TASKS_PROJECT, issue.iid, sort: 'asc')
      .auto_paginate
      .select(&:system)

    start_date = system_notes
      .find { |n| n.body.start_with?('marked the task') }
      &.created_at || issue.created_at

    last_date = system_notes
      .reverse
      .find { |n| n.body.start_with?('marked the task') }
      &.created_at

    end_date =
      if last_date
        Date.parse(last_date)
      elsif issue.closed_at
        Date.parse(issue.closed_at)
      else
        Date.today
      end

    security_weeks.merge(Date.parse(start_date).upto(end_date).map(&:cweek))
  end

(min_week..max_week).each do |week|
  filled_time[week] = per_week_time[week]
  filled_count[week] = per_week_count[week]
end

puts 'Time per week (percentage of total across all weeks):'
puts '-----------------------------------------------------'

filled_time.each do |week, time|
  week_label = week.to_s.ljust(2, ' ')

  line =
    if time.zero?
      week_label
    else
      percentage = ((time / total_mttp) * 100.0)
      blocks = '▮' * percentage.ceil

      "#{week_label} #{blocks} #{percentage.round(2)}%"
    end

  if security_weeks.include?(week)
    puts("\e[33mS #{line}\e[0m")
  else
    puts("  #{line}")
  end
end

puts
puts 'Average MTTP per week'
puts '-----------------------------------------------------'

filled_time.each do |week, time|
  per_week_avg[week] = time / filled_count[week]
end

max_avg = per_week_avg.values.max

per_week_avg.each do |week, average|
  week_label = week.to_s.ljust(2, ' ')
  days = average / 84_400.0

  line =
    if average.zero?
      week_label
    else
      percentage = ((average / max_avg) * 100.0)
      blocks = '▮' * percentage.ceil

      "#{week_label} #{blocks} #{days.round(2)} days"
    end

  if security_weeks.include?(week)
    puts("\e[33mS #{line}\e[0m")
  else
    puts("  #{line}")
  end
end
