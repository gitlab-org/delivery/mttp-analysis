# frozen_string_literal: true
# rubocop: disable all
#
# This script displays the number of merge requests deployed per day. The output
# also indicates if a security release took place, and/or if a new auto-deploy
# branch was created.
#
# If the line starts with "S" it means a security release was active on the
# given day, and the line will be displayed in yellow.
#
# If a line starts with "N" it means a new auto-deploy branch was created on
# that day.

require 'json'
require 'time'
require 'gitlab'
require 'set'

# The project used for (security) release tasks.
RELEASE_TASKS_PROJECT = 'gitlab-org/release/tasks'

com_client = Gitlab::Client.new(
  endpoint: 'https://gitlab.com/api/v4',
  private_token: ENV.fetch('COM_TOKEN')
)

per_date = Hash.new(0)

Dir['./data/pipelines/*.json'].each do |path|
  data = JSON.load(File.read(path))
  deployer = data['deployer']

  next unless deployer['status'] == 'success'

  date = Time.parse(deployer['finished_at']).strftime('%Y-%m-%d')
  amount = deployer['merge_requests'].length
  per_date[date] += amount
end

total_mrs = per_date.values.sum.to_f
sorted = per_date.sort_by { |(key, _)| key }
min_date = sorted[0][0]
max_date = sorted[-1][0]
filled = Hash.new(0)
security_dates = Set.new

com_client
  .issues(
    RELEASE_TASKS_PROJECT,
    labels: 'security,Monthly Release',
    created_after: (Date.parse(min_date) - 7).iso8601,
    confidential: true
  )
  .auto_paginate do |issue|
    # Issues may be created ahead of time or closed later. So instead of relying
    # on creating/closing dates, we rely on when the first/last TODOs are marked
    # as done.
    system_notes = com_client
      .issue_notes(RELEASE_TASKS_PROJECT, issue.iid, sort: 'asc')
      .auto_paginate
      .select(&:system)

    start_date = system_notes
      .find { |n| n.body.start_with?('marked the task') }
      &.created_at || issue.created_at

    last_date = system_notes
      .reverse
      .find { |n| n.body.start_with?('marked the task') }
      &.created_at

    end_date =
      if last_date
        Date.parse(last_date)
      elsif issue.closed_at
        Date.parse(issue.closed_at)
      else
        Date.today
      end

    security_dates.merge(Date.parse(start_date).upto(end_date).map(&:iso8601))
  end

Date.parse(min_date).upto(Date.parse(max_date)).each do |date|
  date_string = date.iso8601
  filled[date_string] = per_date[date_string]
end

last_week = -1

filled.each do |date, amount|
  parsed_date = Date.parse(date)
  day_name = parsed_date.strftime('%A').ljust(10, ' ')
  date_label = date.ljust(11, ' ')

  line =
    if amount.zero?
      "#{day_name} #{date_label}"
    else
      percentage = ((amount / total_mrs) * 100.0)
      blocks = '▮' * percentage.ceil

      "#{day_name} #{date_label} #{blocks} #{percentage.round(2)}%"
    end

  security = security_dates.include?(date)
  prefix =
    if security
      'S'
    # We create auto-deploy branches every Sunday and Wednesday.
    elsif parsed_date.sunday? || parsed_date.wednesday?
      'N'
    else
      ' '
    end

  week = parsed_date.cweek

  if last_week != week
    puts
    puts "------ Week #{week.to_s.ljust(3, ' ')}--------"

    last_week = week
  end

  if security
    puts("\e[33m#{prefix} #{line}\e[0m")
  else
    puts("#{prefix} #{line}")
  end
end
